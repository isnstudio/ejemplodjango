# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Coach, Equipo, Jugador, Juego, JuegoResultado
from django.shortcuts import render, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from forms import EquipoForm ,JugadorForm ,CoachForm, JuegoForm ,JuegoreForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


# for x in xrange(1, 1000):
#     coach = Coach()
#     coach.nombre = "test %s" % (x)
#     coach.edad = x + 18
#     coach.save()


    # contact_list = Contacts.objects.all()
    # paginator = Paginator(contact_list, 25) # Show 25 contacts per page



def home(request):
    template = "home.html"  #viariable string
    equipos = Equipo.objects.all()
    jugadores = Jugador.objects.all()
    coachs = Coach.objects.all()
    paginator = Paginator(coachs, 10)
    page = request.GET.get('page')
    try:
        coachs = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        coachs = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        coachs = paginator.page(paginator.num_pages)

    juegos = Juego.objects.all()
    juegosresultados = JuegoResultado.objects.all() #variable query set
    context ={
        "equipos": equipos,"jugadores": jugadores,"coachs":coachs,
        "juegos":juegos, "juegosresultados":juegosresultados
    }
    return render(request, template, context) #renderizado por medio de una instancia


def agregar_equipos(request):
    template = "agregar.html"
    if request.POST:
        form = EquipoForm(request.POST) 
        if form.is_valid():
            form.save()
            return HttpResponseRedirect (reverse ("home"))
    else:
        form = EquipoForm()
    context ={
        "form":form,
    }

    return render(request,template,context)


def editar_equipos(request, pk):
    detalle = get_object_or_404(Equipo, pk=pk)
    template = "edit.html"
    if request.POST:
        form = EquipoForm(request.POST, instance=detalle)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect (reverse ("home"))
    else:
        form = EquipoForm(instance=detalle)
    context ={
        "detalle":detalle,
        "form":form,
    }
    return render(request,template,context)


def eliminar_equipo(request, pk):
    # Hacer una pantalla de confirmacion
    template = "eliminar.html"
    equipo = get_object_or_404(Equipo, pk=pk)
    if request.POST:
        equipo.delete()
        return render (request ("home"))
    
    return render(request,template)


    
def agregar_jugador(request):
    template = "agregar.html"
    agregar= "agregarju"
    if request.POST:
        form = JugadorForm(request.POST) 
        if form.is_valid():
            form.save()
            return HttpResponseRedirect (reverse ("home"))
        
    else:
        form = JugadorForm()
    context ={
        "form":form ,"agregar":agregar
             }

    return render(request,template,context)

def editar_jugador(request,pk ):
    detalle = get_object_or_404(Jugador, pk=pk)
    template = "edit.html"
    editar= "editarju"
    if request.POST:
        form = JugadorForm(request.POST, instance=detalle) 
        if form.is_valid():
            form.save()
            return HttpResponseRedirect (reverse ("home"))
    else:
        form = JugadorForm(instance=detalle)
    context ={
        "detalle":detalle,"form":form,"editar":editar
        }
    return render(request,template,context)



def eliminar_jugador(request, pk):
    jugador= Jugador.objects.get(pk=pk)
    jugador.delete()
    return HttpResponseRedirect(reverse('home'))


def agregar_coach(request):
    template = "agregar.html"
    agregar = "agregarco"
   # paginate_by = 2
    print agregar
    if request.POST:
        form = CoachForm(request.POST) 
        if form.is_valid():
            form.save()
            return HttpResponseRedirect (reverse ("home"))
        
    else:
        form = CoachForm()
        print "hola"
    context ={
        "form":form ,"agregar":agregar
             }
    return render(request,template,context)


def editar_coach(request,pk ):
    detalle = get_object_or_404(Coach, pk=pk)
    template = "edit.html"
    editar= "editarco"
    if request.POST:
        form = CoachForm(request.POST, instance=detalle) 
        if form.is_valid():
            form.save()
            return HttpResponseRedirect (reverse ("home"))
    else:
        form = CoachForm(instance=detalle)
    context ={
        "detalle":detalle,"form":form,"editar":editar
        }
    return render(request,template,context)

def eliminar_coach(request, pk):
    coach= Coach.objects.get(pk=pk)
    coach.delete()
    return HttpResponseRedirect(reverse('home'))


def agregar_juego(request):
    template = "agregar.html"
    agregar = "agregarjue"
    print agregar
    if request.POST:
        form = JuegoForm(request.POST) 
        if form.is_valid():
            form.save()
            return HttpResponseRedirect (reverse ("home"))
        
    else:
        form = JuegoForm()
        print "hola"
    context ={
        "form":form ,"agregar":agregar
             }
    return render(request,template,context)


def editar_juego(request,pk ):
    detalle = get_object_or_404(Juego, pk=pk)
    template = "edit.html"
    editar= "editarjue"
    if request.POST:
        form = JuegoForm(request.POST, instance=detalle) 
        if form.is_valid():
            form.save()
            return HttpResponseRedirect (reverse ("home"))
    else:
        form = JuegoForm(instance=detalle)
    context ={
        "detalle":detalle,"form":form,"editar":editar
        }
    return render(request,template,context)

def eliminar_juego(request, pk):
    juego = Juego.objects.get(pk=pk)
    juego.delete()
    return HttpResponseRedirect(reverse('home'))

def agregar_juegoresul(request):
    template = "agregar.html"
    agregar = "agregarjuere"
    print agregar
    if request.POST:
        form = JuegoreForm(request.POST) 
        if form.is_valid():
            form.save()
            return HttpResponseRedirect (reverse ("home"))
        
    else:
        form = JuegoreForm()
        print "hola"
    context ={
        "form":form ,"agregar":agregar
             }
    return render(request,template,context)


def editar_juegoresul(request,pk ):
    detalle = get_object_or_404(JuegoResultado, pk=pk)
    template = "edit.html"
    editar= "editarjuere"
    if request.POST:
        form = JuegoreForm(request.POST, instance=detalle) 
        if form.is_valid():
            form.save()
            return HttpResponseRedirect (reverse ("home"))
    else:
        form = JuegoreForm(instance=detalle)
    context ={
        "detalle":detalle,"form":form,"editar":editar
        }
    return render(request,template,context)

def eliminar_juegoresul(request, pk):
    juegore = JuegoResultado.objects.get(pk=pk)
    juegore.delete()
    return HttpResponseRedirect(reverse('home'))