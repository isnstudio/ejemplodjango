# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models import Coach, Equipo, Jugador, Juego, JuegoResultado
 

# Register your models here.
admin.site.register(Coach)
admin.site.register(Equipo)
admin.site.register(Jugador)
admin.site.register(Juego)
admin.site.register(JuegoResultado)

