# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Coach(models.Model):
    nombre = models.CharField(max_length=150)
    edad = models.IntegerField()
        
    class Meta:
        verbose_name = "Coach"
        verbose_name_plural = "Coachs"

    def __str__(self):
        return self.nombre

class Equipo(models.Model):
    coach = models.ForeignKey(Coach)
    nombre = models.CharField(max_length=150)
    class Meta:
        verbose_name = "Equipo"
        verbose_name_plural =   "Equipos"
    
    def __str__(self):
        return self.nombre
    
class Jugador(models.Model):
    equipo = models.ForeignKey(Equipo, null=True, blank=True)
    nombre = models.CharField(max_length=150)
    edad = models.IntegerField()
    class Meta:
        verbose_name = "Jugador"    
        verbose_name_plural = "Jugadores"
    
    def __str__(self):
        return self.nombre


class Juego(models.Model):
    donde = models.CharField(max_length=150)
    tiempo = models.DateTimeField()
    
    class Meta:
        verbose_name = "Juego"
        verbose_name_plural = "Juegos"

    def __str__(self):
        return self.donde
    
class JuegoResultado(models.Model):
    juego = models.ForeignKey(Juego)
    equipo =models.ForeignKey(Equipo) 
    puntos = models.IntegerField() 
    
    class Meta:
        verbose_name = "JuegoResultado"
        verbose_name_plural = "JuegoResultados"

    def __str__(self):
        return self.juego.donde
