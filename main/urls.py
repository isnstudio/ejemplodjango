from django.contrib import admin
from django.conf.urls import include, url
import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^agregar/equipo', views.agregar_equipos, name='agregar'),
    url(r'^editar/equipo/(?P<pk>[0-9]+)/$', views.editar_equipos, name='editar'),
    url(r'^eliminar/equipo/(?P<pk>[0-9]+)/$', views.eliminar_equipo, name='eliminar'),
    url(r'^agregar/jugador/', views.agregar_jugador, name='agregarju'),
    url(r'^editar/jugador/(?P<pk>[0-9]+)/$', views.editar_jugador, name='editarju'),
    url(r'^eliminar/jugador/(?P<pk>[0-9]+)/$', views.eliminar_jugador, name='eliminarju'),
    url(r'^agregar/Coach/', views.agregar_coach, name='agregarco'),
    url(r'^editar/Coach/(?P<pk>[0-9]+)/$', views.editar_coach, name='editarco'),
    url(r'^eliminar/Coach/(?P<pk>[0-9]+)/$', views.eliminar_coach, name='eliminarco'),
    url(r'^agregar/Juego/', views.agregar_juego, name='agregarjue'),
    url(r'^editar/Juego/(?P<pk>[0-9]+)/$', views.editar_juego, name='editarjue'),
    url(r'^eliminar/Juego/(?P<pk>[0-9]+)/$', views.eliminar_juego, name='eliminarjue'),
    url(r'^agregar/JuegoResultado/', views.agregar_juegoresul, name='agregarjuere'),
    url(r'^editar/JuegoResultado/(?P<pk>[0-9]+)/$', views.editar_juegoresul, name='editarjuere'),
    url(r'^eliminar/JuegoResultado/(?P<pk>[0-9]+)/$', views.eliminar_juegoresul, name='eliminarjuere'),
]
