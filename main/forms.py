from django import forms
from .models import Coach, Equipo, Jugador, Juego, JuegoResultado


class EquipoForm(forms.ModelForm):

    class Meta:
        model = Equipo
        fields = ('nombre', 'coach',)

class JugadorForm(forms.ModelForm):

    class Meta:
        model = Jugador
        fields = ('equipo', 'nombre','edad')

class CoachForm(forms.ModelForm):

    class Meta:
        model = Coach
        fields = ('nombre','edad')

class JuegoForm(forms.ModelForm):

    class Meta:
        model = Juego
        fields = ('donde','tiempo')

class JuegoreForm(forms.ModelForm):

    class Meta:
        model = JuegoResultado
        fields = ('juego','equipo','puntos')

